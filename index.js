const auth = require('./auth');
const events = require('./events');

module.exports = {
    login: auth.login,
    fetchEvents: events.fetchEvents,
    fetchEventsForDivision: events.fetchEventsForDivision,
    fetchRegions: events.fetchRegions,
}
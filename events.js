const moment = require('moment-timezone');
const net = require('./net');
const $ = require('cheerio');

/**
 * MSJ represents dates in the format '21-Oct-2018 15:00'.
 */
const parseEventDate = (dateString) => {
    return moment.tz(dateString, 'DD-MMM-YYYY HH:mm', 'Australia/Melbourne').toDate()
};

/**
 * Take a jsdom "table" element, and turn each row into an object, indexed by the labels in the "th" cells.
 */
const parseTable = (dom) => {

    const headers = dom.find("thead th").map((i, th) => $(th).text()).get();

    const values = dom.find("tbody tr").map((i, tr) => {

        const cells = $(tr).find('td').map((i, td) => $(td).text().trim().replace(/\s\s+/g, ' '));

        const rowData = {};
        for (let i = 0; i < cells.length; i ++) {
            rowData[headers[i]] = cells[i];
        }

        return rowData;

    }).get();

    // Some tables (such as "My Hours" include rows that represent subtotals of all rows above them.
    // Exclude these, because they don't have any particular relevance in most cases.
    return values.filter(event => Object.values(event).length === headers.length);
};

const parseEventFromFutureEvents = (event) => ({
    pdn: event['PDN'],
    name: event['Event Name'],
    startDate: parseEventDate(event['Start Date']),
    endDate: parseEventDate(event['End Date']),
    status: event['Status'],
    venueName: event['Venue Name'],
    venueAddress: event['Venue Address'],
    region: event['Region'],
    rostered: event['Action'] === 'Submit EOI' ? null : event['Action'],
});

const parseEventFromMyRoster = (event) => ({
    pdn: event['PDN'],
    name: event['Event Name'],
    startDate: parseEventDate(event['Start Date']),
    endDate: parseEventDate(event['End Date']),
    status: event['Status'],
    venueName: event['Venue Name'],
    region: event['Region'],
    rostered: event['Status'],
});

const parseEventFromMyHours = (event) => ({
    pdn: event['PDN'],
    name: event['Event Name'],
    startDate: parseEventDate(event['Start Date']),
    endDate: parseEventDate(event['End Date']),
    venueName: event['Venue'],
    organiser: event['Organiser'],
    hours: parseFloat(event['Hours']),
});

const parseEventsTableFromDom = (dom, eventParser) => {
    const eventsTable = dom.find(".pgAidKits");
    const eventsDataFromTable = parseTable(eventsTable);
    return eventsDataFromTable.map(eventParser);
};

const parseDivisionIdsFromDom = (dom) => {
    const options = dom.find('select[name=division_id] option');
    return options.map(
        (i, option) => ({
            id: parseInt($(option).val()),
            name: $(option).text().trim()
        })
    ).get();
};

const parseDom = (dom, eventParser) => ({
    availableDivisions: parseDivisionIdsFromDom(dom),
    events: parseEventsTableFromDom(dom, eventParser)
});

const fetchEvents = async (sessionId) => {
    const dom = await net.get('http://ssl.stjohnvic.com.au/msj/event/list.jsp', sessionId);
    return parseDom(dom, parseEventFromFutureEvents);
};

const fetchEventsForDivision = async (sessionId, divisionId) => {
    const dom = await net.post(
        'http://ssl.stjohnvic.com.au/msj/event/list.jsp?action=init&sort=e_startdate&sort_type=desc&pg_number=1',
        sessionId,
        { division_id: divisionId }
    );

    return parseDom(dom, parseEventFromFutureEvents);
};

const fetchEventsFromMyRoster = async (sessionId) => {
    const dom = await net.get('http://ssl.stjohnvic.com.au/msj/event/upcoming.jsp', sessionId);
    return parseDom(dom, parseEventFromMyRoster);
};

/**
 * Returns a Array of regions in the format {id: ID, name: NAME}.
 * This is quite wasteful though, because it pulls down the entire list of events just to ask for the divisions.
 * If you care about how many requests you make, then just fetch a list of events for your region using "fetchEvents",
 * and then the resulting data will contain 'availableDivisions'.
 */
const fetchRegions = async (sessionId) => {
    const eventsData = await fetchEvents(sessionId);
    return eventsData.availableDivisions;
};

const fetchMyHours = async (sessionId) => {
    const dom = await net.get('http://ssl.stjohnvic.com.au/msj/event/hours.jsp', sessionId);
    return parseDom(dom, parseEventFromMyHours);
};

module.exports = {
    fetchRegions: fetchRegions,
    fetchEvents: fetchEvents,
    fetchEventsForDivision: fetchEventsForDivision,
    fetchEventsFromMyRoster: fetchEventsFromMyRoster,
    fetchMyHours: fetchMyHours,
};
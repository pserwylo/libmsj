const axios = require('axios');
const cheerio = require('cheerio');
const querystring = require('query-string');

const getMsjPage = (url, sessionId) => {
    return axios.get(url, {
        headers: {
            'Cookie': `JSESSIONID=${sessionId}`
        }
    });
};

const postMsjPage = (url, sessionId, data) => {
    return axios.post(url, querystring.stringify(data), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Cookie': `JSESSIONID=${sessionId}`,
        }
    });
};

const responseToDom = async (response) => {
    const body = await response.data;
    const $ = cheerio.load(body);

    const usernameInput = $('input[name=j_username]');

    if (usernameInput.length > 0) {
        throw new SessionExpiredError();
    }

    return $('body');
};

const get = async (url, sessionId) => {
    const response = await getMsjPage(url, sessionId);
    return responseToDom(response);
};

const post = async (url, sessionId, data) => {
    const response = await postMsjPage(url, sessionId, data);
    return responseToDom(response);
};

class SessionExpiredError extends Error {
    constructor() {
        super();
        this.name = "SessionExpiredError";
    }
}

module.exports = {
    get: get,
    post: post
};
const axios = require('axios');
const querystring = require('query-string');

const sessionIdFromHeaders = (headers) => {
    if (!headers.hasOwnProperty('set-cookie') || !(headers['set-cookie'] instanceof Array) || headers['set-cookie'].length === 0) {
        return null;
    }

    const match = headers['set-cookie'][0].match(/.*JSESSIONID=(\w*);.*/);
    return match.length > 1 ? match[1] : null;
};

const fetchNewSessionIdFromServer = async () => {
    const data = await axios.get('http://ssl.stjohnvic.com.au/msj/');
    return sessionIdFromHeaders(data.headers);
};

const login = async (username, password) => {

    const initialSessionId = await fetchNewSessionIdFromServer();

    const data = {
        j_username: username,
        j_password: password,
    };

    const response = await axios.post('http://ssl.stjohnvic.com.au/msj/j_security_check', querystring.stringify(data), {
        maxRedirects: 1, // We expect to be redirected to a new page, and receive a new session cookie in response.
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Cookie': `JSESSIONID=${initialSessionId}`,
        },
    });

    return sessionIdFromHeaders(response.headers);

};

module.exports = { login: login };